<?PHP

include ( "common.php" ) ;

print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<STYLE TYPE="text/css" MEDIA=screen>
<!--
  .class1 { background-color : #EEEEFF ; text-align : center }
  .class2 { background-color : #EEEEEE ; text-align : center }
-->
</STYLE>
</head>' ;
print get_common_header ( "amalglamate.php" ) ;
print "<h1>Amal<i>GLAM</i>ate</h1>" ;

$p = file_get_contents ( '/home/magnus/.my.cnf' ) ;
$p = array_pop ( explode ( 'password' , $p , 2 ) ) ;
$p = explode ( '"' , $p ) ;
$password = $p[1] ;

$db = 'u_magnus_glam_p' ;
$mysql_con = mysql_connect ( 'sql' , 'magnus' , $password ) ;
mysql_select_db ( $db , $mysql_con ) ;

$sql = "SELECT * FROM summaries ORDER BY timestamp" ;
$res = mysql_query ( $sql , $mysql_con ) ;
print mysql_error();

$data = array () ;
$groups = array () ;
while ( $o = mysql_fetch_object ( $res ) ) {
	$data[$o->timestamp][$o->category] = $o ;
	$groups[$o->category] = 1 ;
}

function get_diff ( $cur , $last ) {
	if ( $cur == $last ) return '<br/>(&plusmn;0)' ;
	$ad = $cur - $last ;
	$rd = $ad * 100 / $last ;
	if ( $ad > 0 ) $col = '#008A00' ;
	else $col = '#8A0000' ;
	return sprintf ( "<br/>(<span style='color:$col'>%+d</span>/<span style='color:$col'>%+2.1f%%</span>)" , $ad , $rd ) ;
}

$gc = count ( $groups ) ;
print "<p>For each of the $gc group (changes to previous timepoint are shown, if available) :
<ul>
<li>Total images in group on Commons</li>
<li>Distinct files used in projects (% of total images in group)</li>
<li>Total uses of files in projects (average uses per image)</li>
</ul></p>" ;

print "<div style='position:absolute;left:0px;right:0px;overflow:auto'><table border='1' style='font-size:80%'>" ;

$cls = array () ;
$cl = 'class1' ;
print "<tr><th rowspan='2'>Date</th>" ;
foreach ( $groups AS $g => $dummy ) {
	if ( $cl == 'class1' ) $cl = 'class2' ;
	else $cl = 'class1' ;
	$cls[$g] = $cl ;
	print "<th class='$cl' colspan='3'><a href='http://toolserver.org/~magnus/glamorous.php?doit=1&category=" . urlencode ( $g ) . "&use_globalusage=1&ns0=1&show_details=1'>$g</a></th>" ;
}
print "</tr>" ;

print "<tr>" ;
foreach ( $groups AS $g => $dummy ) {
	$c = " class='".$cls[$g]."' " ;
	print "<th $c>&sum;</th>" ;
	print "<th $c>Used</th>" ;
	print "<th $c>Uses</th>" ;
}
print "</tr>" ;

$last = array () ;
foreach ( $data AS $ts => $d1 ) {
	$year = substr ( $ts , 0 , 4 ) ;
	$month = substr ( $ts , 4 , 2 ) ;
	$day = substr ( $ts , 6 , 2 ) ;
	
	$date = "$year-$month-$day" ;
	$days = floor(mktime(1,1,1,$month, $day, $year)/86400); 
	if ( isset ( $last_days ) ) {
		$daydiff = round(($days - $last_days), 0); 
		$date .= "<br/>$daydiff days" ;
	}
	
	print "<tr>" ;
	print "<th>$date</th>" ;
	foreach ( $groups AS $g => $dummy ) {
		$c = " nowrap class='".$cls[$g]."' " ;
		if ( !isset ( $d1[$g] ) ) {
			print "<td $c colspan='3'>N/A</td>" ;
			if ( isset ( $last[$g] ) ) unset ( $last[$g] ) ;
			continue ;
		}
		$d2 = $d1[$g] ;
		$fic = $d2->files_in_category_tree ;
		$dfu = $d2->distinct_files_used ;
		$tfu = $d2->total_files_usage ;
		
		$t1 = $fic ;
		$t2 = sprintf ( "%d (%2.1f%%)" , $dfu , $dfu * 100 / $fic ) ;
		$t3 = sprintf ( "%d (%2.2f&times;)" , $tfu , $tfu / $dfu ) ;
		
		if ( isset ( $last[$g] ) ) {
			$t1 .= get_diff ( $fic , $last[$g][0] ) ;
			$t2 .= get_diff ( $dfu , $last[$g][1] ) ;
			$t3 .= get_diff ( $tfu , $last[$g][2] ) ;
		}
		
		$last[$g] = array ( $fic , $dfu , $tfu ) ;
		
		print "<td $c>$t1</td>" ;
		print "<td $c>$t2</td>" ;
		print "<td $c>$t3</td>" ;
	}
	print "</tr>" ;
	$last_days = $days ;
}

print "</table></div>" ;

print "</body></html>" ;

?>
