var min_month_hide_details = 5 ;
var currentTime = new Date()
var default_year = currentTime.getFullYear() ;
var default_month = currentTime.getMonth() + 1 ;

var running = false ;
var catlist = [] ;
var results = {} ;

function setDateRange ( y1 , m1 , d1 , y2 , m2 , d2 ) {
	$('#start_date').html ( y1 + '-' + padMonthDay ( m1 ) ) ;
	$('#start_day').val ( d1 ) ;
	$('#end_date').html ( y2 + '-' + padMonthDay ( m2 ) ) ;
	$('#end_day').val ( d2 ) ;
}

function showExample ( id ) {
	clearCategories() ;
	$('#lang').html ( 'en' ) ;
	$('#project').html ( 'wikipedia' ) ;
	if ( id == 1 ) {
		setDateRange ( 2015 , 10 , 1 , 2015 , 10 , 31 ) ;
		$('#depth').val ( 9 ) ;
		addCategory ( { name:'Indianapolis, Indiana' } ) ;
		addCategory ( { name:'Public art' } ) ;
		runTool();
	} else if ( id == 2 ) {
		setDateRange ( 2015 , 10 , 1 , 2015 , 10 , 31 ) ;
		$('#depth').val ( 9 ) ;
		addCategory ( { name:'British Museum' } ) ;
		addCategory ( { exclude:true , name:'Natural History Museum' } ) ;
		addCategory ( { exclude:true , name:'British Museum directors' } ) ;
		addCategory ( { exclude:true , name:'Employees of the British Museum' } ) ;
		addCategory ( { exclude:true , name:'Trustees of the British Museum' } ) ;
		runTool();
	} else if ( id == 3 ) {
		setDateRange ( 2015 , 10 , 1 , 2015 , 10 , 31 ) ;
		$('#depth').val ( 9 ) ;
		addCategory ( { talk:true , name:'World War I articles by quality' } ) ;
		runTool();
	}
}

function clearCategories () {
	catlist = [] ;
	$('.catrow').remove() ;
	updateStatus() ;
}

function removeCategory ( row ) {
	catlist[row].useit = false ;
	$('#catrow'+row).remove() ;
	updateStatus() ;
}

function updateStatus() {
	if ( running ) {
		$('#theform input').attr('disabled','disabled') ;
		$('#theform button').attr('disabled','disabled') ;
		return ;
	} else {
		$('#theform input').removeAttr('disabled') ;
		$('#theform button').removeAttr('disabled') ;
		bootstrap4_compatability();
	}

	if ( $('.catrow').length == 0 ) {
		$('#b_run').hide() ;
		$('#b_clear').hide() ;
		$('#cat_header').hide() ;
	} else {
		$('#b_run').show() ;
		$('#b_clear').show() ;
		$('#cat_header').show() ;
	}
	
}

function updateRowData ( row ) {
	catlist[row].lang = $('#catlang'+row).val() ;
	catlist[row].project = $('#catproject'+row).val() ;
	catlist[row].exclude = $('#catexclude'+row+':checked').val() !== undefined ? true : false ;
	catlist[row].talk = $('#talk'+row+':checked').val() !== undefined ? true : false ;
	catlist[row].name = $('#catname'+row).val() ;
	catlist[row].depth = $('#catdepth'+row).val() ;
}

function addCategory ( o ) {
	o.row = catlist.length ;
	o.useit = true ;
	o.exclude = o.exclude || false ;
	o.talk = o.talk || false ;
	$.each ( [ 'lang' , 'project' , 'name' , 'depth' ] , function ( k , v ) {
		o[v] = o[v] || '' ;
	} ) ;
	catlist.push ( o ) ;
	var h = '' ;
	h += "<tr id='catrow" + o.row + "' class='catrow'>" ;
	h += "<td><input id='catlang"+o.row+"' type='text' class='input-small span1' value='"+o.lang+"' /></td>" ;
	h += "<td><input id='catproject"+o.row+"' type='text' class='input-small span1' value='"+o.project+"' /></td>" ;
	h += "<td style='text-align:center'><input id='catexclude"+o.row+"' type='checkbox' " + (o.exclude?'checked':'') + "/></td>" ;
	h += "<td style='text-align:center'><input id='talk"+o.row+"' type='checkbox' " + (o.talk?'checked':'') + "/></td>" ;
	h += "<td><input id='catname"+o.row+"' type='text' class='input-small span4' value='"+o.name+"'/></td>" ;
	h += "<td><input id='catdepth"+o.row+"' type='number' class='catdepth input-small span1' value='"+o.depth+"'/></td>" ;
	h += "<td><a href='#' onclick='removeCategory("+o.row+")' title='Remove category'><b>&times;</b></a></td>" ;
	h += "</tr>" ;

	$('#catable tbody').append ( h ) ;
	$('#catrow'+o.row+' input').change ( function () { updateRowData ( o.row ) } ) ;
	$('#catname'+o.row).focus() ;
	updateStatus() ;
}

function getTotalQuery () {
	var q = {
		autolang : ( $('#languages:checked').val() !== undefined ) ,
		union : ( $('#union:checked').val() !== undefined ) ,
		dates : ( $('#dates:checked').val() !== undefined ) ,
		files : ( $('#files:checked').val() !== undefined ) ,
		lang : $('#lang').val() ,
		project : $('#project').val() ,
		pagepile : $('#pagepile').val() ,
		depth : $('#depth').val()
	} ;
	
	q.rows = [] ;

	$.each ( catlist , function ( row , v ) {
		if ( !v.useit ) return ;
		if ( v.name == '' ) return ;
		var o = {
			title : v.name ,
			lang : v.lang == '' ? ($('#lang').val()||$('#lang').attr('placeholder')) : v.lang ,
			project : v.project == '' ? ($('#project').val()||$('#project').attr('placeholder')) : v.project ,
			exclude : v.exclude ,
			talk : v.talk ,
			depth : v.depth == '' ? ($('#depth').val()||$('#depth').attr('placeholder')) : v.depth
		} ;
		
		q.rows.push ( o ) ;
		
	} ) ;
		
	return q ;
}

var defaults = {
	depth : 9 ,
	lang : 'en' ,
	project : 'wikipedia' ,
	pagepile : ''
}

function showPermalink ( q ) {
	
	// Simplify query
	$.each ( [ 'autolang' , 'union' , 'dates' , 'files' ] , function ( k , v ) {
		if ( !q[v] ) delete q[v] ;
	} ) ;
	$.each ( defaults , function ( k , v ) {
		if ( q[k] == v || q[k] == '' ) delete q[k] ;
	} ) ;
	$.each ( q.rows , function ( k , v ) {
		if ( v.lang == '' || v.lang == defaults.lang ) delete v.lang ;
		if ( v.project == '' || v.project == defaults.project ) delete v.project ;
		if ( v.depth == '' || v.depth == (q.depth||defaults.depth) ) delete v.depth ;
		if ( !v.exclude ) delete v.exclude ;
		if ( !v.talk ) delete v.talk ;
	} ) ;
	
	// Build link
	var s = encodeURIComponent ( JSON.stringify ( q ) ) ;
	var url = "?q=" + s ;
	var link = "<a href='" + url + "'>Permalink</a> to this query</a>" ;
	$('#results').append("<div style='margin-bottom:10px;'>"+link+"</div>") ;
}

function runTool() {
	running = true ;
	results = {} ;
	updateStatus() ;
	$('#results').html('') ;
	$('#result_container').show() ;
	$('input').blur() ;
	clearSelection();
	$('#b_run').button('toggle').focus() ;

	var query = getTotalQuery () ;
	showPermalink ( clone ( query ) ) ;

	// Logging
	$.getJSON ( 'https://tools.wmflabs.org/magnustools/logger.php?tool=treeviews&method=run&callback=?' , function(j){} ) ;

	if ( query.autolang ) {
		$('#results').append ( "<div class='tmp_loading' id='temp_language_links'><span class='label label-info'><i>Checking language links...</i></span></div>" ) ;
		var tmp = [] ;
		$.each ( query.rows , function ( k , v ) {
			tmp.push ( clone ( v ) ) ;
		} ) ;
		var ll_run = tmp.length ;

		$.each ( tmp , function ( k , v ) {
			v.ns = 14 ;
			var cat = new WikiPage ( v ) ;
			cat.getLanguageLinks ( function ( o ) {
				$.each ( o.langlinks , function ( k2 , v2 ) {
					var title = v2.replace ( /^[^:]+:/ , '' ) ;
					query.rows.push ( { title:title , lang:k2 , project:v.project , exclude:v.exclude , talk:v.talk , depth:v.depth } ) ;
				} ) ;
				ll_run-- ;
				if ( ll_run > 0 ) return ;
				runTool2 ( query ) ;
			} ) ;
		} ) ;
	} else {
		runTool2 ( query ) ;
	}
}

function runTool2 ( query ) {
	var list = [] ;
	var cache = {} ;
	$.each ( query.rows , function ( k , v ) {
		cache[v.lang+'|'+v.project] = { lang : v.lang , project : v.project } ;
	} ) ;
	$.each ( cache , function ( k , v ) {
		list.push ( v ) ;
	} ) ;
	
	var pagepile = $('#pagepile').val() * 1 ;
	if ( pagepile > 0 ) return runTool3pagepile ( pagepile , query ) ;
	
	wikiDataCache.ensureSiteInfo ( list , function () { runTool3 ( query ) ; } ) ;
}

function runTool3pagepile ( pagepile , query ) {
	$.getJSON ( '/pagepile/api.php?id=' + pagepile + '&action=get_data&format=json&doit&callback=?' , function ( d ) {
		if ( d.wiki == 'wikidatawiki' ) { alert ( "Does not work for " + d.wiki + ' yet' ) ; return ; }
		var m = d.wiki.match ( /^(.+)(wik.+)$/ ) ;
		if ( m == null ) { alert ( "Cannot parse " + d.wiki ) ; return ; }
		query.lang = m[1] ;
		query.project = m[2] == 'wiki' ? 'wikipedia' : m[2] ;
		
		wikiDataCache.ensureSiteInfo ( [ { lang:query.lang , project:query.project } ] , function () {
			var lp = query.lang + '.' + query.project ;
			if ( undefined === results[lp] ) results[lp] = [] ;
		
			// Rewrite keys from page_id to title-thing. Necessary because of talk option.
			var d2 = {} ;
			$.each ( d.pages , function ( k , v ) {
				var k2 = lp + '|' + v ;
				var p = new WikiPage ( { title : v , ns : o , lang : query.lang , project : query.project } ) ;
				d2[k2] = p ;
			} ) ;
		
			var o = new WikiPage ;
			o.lang = query.lang ;
			o.project = query.project ;
			results[lp].push ( { root : o , row : 0 , data : d2 } ) ;
		
			subsetData () ;

		} ) ;
	} ) ;
}

function runTool3 ( query ) {
	var my_status = {} ;
	var files = $('#files:checked').val() !== undefined ? true : false ;
	my_status.cats_running = query.rows.length ;
	$('#temp_language_links').remove();
	
	if ( 0 == query.rows.length ) {
		$("#results").html ( "<i>No results</i>" ) ;
		running = false ;
		updateStatus() ;
		$('#b_run').button('toggle') ;
		return ;
	}
	
	var ns = files ? 6 : 0 ;
	
	$.each ( query.rows , function ( row , v ) {
		var o = new WikiPage ;
		o.title = v.title ;
		o.ns = 14 ; // Category
		o.lang = v.lang ;
		o.project = v.project ;
		o.exclude = v.exclude ;
		o.talk = v.talk ;
		o.depth = v.depth ;
		
		$('#results').append ( "<div class='tmp_loading' id='temp"+row+"'><b>"+o.lang+"."+o.project+": "+o.title+"</b> <span id='pagesfound"+row+"' class='label label-info'><i>Loading...</i></span></div>" ) ;

		o.getPagesInCategoryTree ( { depth : o.depth , namespaces : [ ns+(o.talk?1:0) ] , cacheable : true , callback : function ( d ) {
			var l = 0 ;
			$.each ( d , function ( k , v ) { l++ } ) ;
			$('#pagesfound'+row).html ( l + ' pages found' ) ;

			var lp = o.lang + '.' + o.project ;
			if ( undefined === results[lp] ) results[lp] = [] ;
			
			if ( o.talk ) { // Changing namespaces. Page IDs will be wrong, but it should still work. I hope.
				$.each ( d , function ( k , v ) {
					v.namespace_name = '' ;
					v.ns = 0 ;
				} ) ;
			}
			
			// Rewrite keys from page_id to title-thing. Necessary because of talk option.
			var d2 = {} ;
			$.each ( d , function ( k , v ) {
				var k2 = lp + '|' + v.title ;
				d2[k2] = v ;
			} ) ;
			
			results[lp].push ( { root : o , row : row , data : d2 } ) ;
			my_status.cats_running-- ;
			if ( 0 == my_status.cats_running ) subsetData () ;
			
		} } ) ;
	} ) ;

}

var viewstack = {} ;
var sum_views = {} ;
var querying = {} ;
var total_querying ;
var languages ;

function subsetData () {
	$('#results').append ( "<div id='subsets'></div>" ) ;

	viewstack = {} ;
	var union = $('#union:checked').val() !== undefined ? true : false ;
	var dates = $('#dates:checked').val() !== undefined ? true : false ;
	total_querying = 0 ;
	var language_counter = 0 ;
	querying = {} ;
	languages = 0 ;
	$.each ( results , function ( lp , v1 ) {
		languages++ ;
	} ) ;

	sum_views = {} ;

	var months = [] ;
	var d1 = $('#start_date').text() + '-' + padMonthDay($('#start_day').val()) ;
	var d2 = $('#end_date').text() + '-' + padMonthDay($('#end_day').val()) ;
	while ( d1 < d2 ) {
		var y = d1.substr(0,4) * 1 ;
		var m = d1.substr(5,2) * 1 ;
		var d = d1.substr(8,2) * 1 ;
		var o = { year:y , month:m , start_day:d } ;
		if ( d1.substr(0,7) == d2.substr(0,7) ) o.end_day = d2.substr(8,2) * 1 ;
		else o.end_day = 31 ;
		o.date = y+''+padMonthDay(m) ;
		months.push ( o ) ;
		m++ ;
		if ( m == 13 ) {
			m = 1 ;
			y++ ;
		}
		d1 = y + '-' + padMonthDay(m) + '-01' ;
	}


	$.each ( results , function ( lp , v1 ) {
		language_counter++ ;
		var lcnt = language_counter ;
		var pages ;
		var lang = v1[0].root.lang ;
		var project = v1[0].root.project ;
		
		// Include
		$.each ( v1 , function ( k2 , v2 ) {
			if ( v2.root.exclude ) return ;
			if ( undefined === pages ) { // First set
				pages = clone ( v2.data ) ;
				return ;
			}
			
			if ( union ) {
				$.each ( v2.data , function ( k3 , v3 ) {
					if ( undefined === pages[k3] ) pages[k3] = clone ( v3 ) ;
				} ) ;
			} else { // Subset; default
				$.each ( pages , function ( k3 , v3 ) {
					if ( undefined === v2.data[k3] ) delete pages[k3] ; // Subset only
				} ) ;
			}
		} ) ;
		
		if ( undefined === pages ) pages = {} ; // Paranoia

		// Exclude
		$.each ( v1 , function ( k2 , v2 ) {
			if ( !v2.root.exclude ) return ;
			$.each ( v2.data , function ( k3 , v3 ) {
				if ( undefined !== pages[k3] ) delete pages[k3] ;
			} ) ;
		} ) ;
		
		querying[lp] = 0 ;
		sum_views[lp] = 0 ;

		var h = "<h4 style='margin-top:5px'><div id='spinner"+lcnt+"' style='float:right'><img src='"+wikiSettings.images.spin+"' /></div>" + lang + "." + project + "</h4>" ;
		h += "<div>" ;
		h += "<span class='label label-info'><span class='total_pages'>" + prettyNumber ( Object.size ( pages ) ) + "</span> pages total</span> " ;
		h += "<span class='label label-info'><span class='total_nonzero' id='nonzero"+lcnt+"'>0</span> pages with views</span> " ;
		h += "<span class='label label-info'><span class='total_views' id='totalviews"+lcnt+"'>0</span> total views</span> " ;
		h += "<button class='btn btn-mini' onclick='$(\"#table"+lcnt+" .zero_views\").toggle();return false'>Toggle zero-views</button>" ;
		if ( languages > 1 ) h += " <button class='btn btn-mini' onclick='$(\"#table"+lcnt+"\").toggle();return false'>Toggle table</button>" ;
		if ( months.length >= min_month_hide_details ) h += " <button class='btn btn-mini' onclick='$(\".detail_cell\").toggle();return false'>Toggle details</button>" ;
		h += "</div>" ;
		
		h += "<table id='table"+lcnt+"' class='table table-condensed tablesorter'" ;
		if ( languages > 1 ) h += " style='display:none'"
		h += ">" ;
		h += "<thead><tr><th style='width:100%'>Page</th>" ;
		
		$.each ( months , function ( k3 , v3 ) {
			h += "<th class='detail_cell' style='padding-right:20px' nowrap>" + v3.year + "-" + padMonthDay(v3.month) + "</th>" ;
		} ) ;
		if ( months.length > 1 ) h += "<th style='padding-right:20px' nowrap>Total views</th>" ;
		
		if ( dates ) h += "<th class=\"{sorter: 'text'}\" nowrap>Creation date</th>" ;
		h += "</tr></thead><tbody>" ;
		$.each ( pages , function ( k2 , v2 ) {
			h += "<tr>" ;
			h += "<td>" + v2.getLink ( { target:'_blank' } ) + "</td>" ;
			$.each ( months , function ( k3 , v3 ) {
				h += "<td class='detail_cell' style='text-align:right;font-family:courier'><span id='views"+lcnt+"_"+sanitizeID(k2) + "_" + v3.date + "'>?</span></td>" ;
			} ) ;
			if ( months.length > 1 ) h += "<td style='text-align:right;font-family:courier'><span id='views"+lcnt+"_"+sanitizeID(k2) + "_sum" + "'>?</span></td>" ;
			if ( dates ) h += "<td id='creationdate"+lcnt+"_"+k2 + "' style='font-family:courier;font-size:9pt'>????-??-??</td>" ;
			h += "</tr>" ;
			querying[lp]++ ;
			total_querying++ ;
			if ( dates ) { querying[lp]++ ; total_querying++ ; }
		} ) ;
		h += "</tbody><tfoot><tr><td>Total</td>" ;
		$.each ( months , function ( k2 , v2 ) {
			h += "<td class='detail_cell' style='text-align:right;font-family:courier'><span id='views_total"+lcnt+"_"+v2.date+"'>?</span></td>" ;
		} ) ;
		if ( months.length > 1 ) h += "<td style='text-align:right;font-family:courier'><span id='views_total"+lcnt+"'>?</span></td>" ;
		if ( dates ) h += "<td></td>" ;
		h += "</tr></tfoot></table>" ;
		$('#subsets').append ( h ) ;

		$.each ( pages , function ( k2 , v2 ) {
			
			if ( dates ) {
				v2.getCreationDate ( function ( o ) {
					$('#creationdate'+lcnt+"_"+k2).html ( o.creationdate.substr(0,10) ) ;
					querying[lp]-- ;
					total_querying-- ;
					if ( querying[lp] == 0 ) check_queue ( lcnt , sum_views[lp] , total_querying ) ;
				} ) ;
			}
			
			var pagekey = lang + wikiDataCache.pv_proj2stats[project] + ':' + v2.getFullNiceTitle() ;
			viewstack[pagekey] = {
//				date : date ,
				lang : lang ,
				project : project ,
				title : v2.getFullNiceTitle() ,
				lcnt : lcnt ,
				k2 : k2 ,
				lp : lp
			} ;

		} ) ;

	} ) ;
	
	if ( months.length >= min_month_hide_details ) {
		$('.detail_cell').hide() ;
	}
	
	$('.tmp_loading').remove() ;
	$('#subsets').append ( "<div id='totals'></div>" ) ;
	
	$.each ( viewstack , function ( k , v ) {
		var page = new WikiPage ( v ) ;
		$.each ( months , function ( k2 , v2 ) {
			var o = jQuery.extend(true, {}, v2);
			o.callback = myPageviewsCallback ;
//			pageViewHack ( o , page ) ;
			page.getViewStats ( o ) ;
		} ) ;
	} ) ;
	
}

function padMonthDay ( md ) {
	return md < 10 ? '0'+md : ''+md ;
}


function myPageviewsCallback ( result ) {
	var d = result.data ;
	var pagekey = d.lang + ":" + d.title ;
	var o = viewstack[pagekey] ;
/*	if ( typeof o == 'undefined' ) {
		console.log ( "Not found:" , pagekey , result ) ;
		return ;
	}*/
	var sd = result.options.start_day ;
	var ed = result.options.end_day ;

	var views = 0 ;
	if ( undefined !== d && undefined !== d.daily_views ) {
		$.each ( d.daily_views , function ( k3 , v3 ) {
			var day = k3.substr ( 8 , 2 ) * 1 ;
			if ( day < sd || day > ed ) return ;
			views += parseInt ( v3 ) ;
		} ) ;
	}
	sum_views[o.lp] += views ;
	
	var link_url = "http://"+wikiSettings.stats_grok+"/"+o.lang+"/"+result.options.date+"/"+encodeURIComponent(o.title) ;
	if ( result.options.date >= '201509' ) {
		var year = result.options.date.substr(0,4) ;
		var month = result.options.date.substr(4,2) ;
		link_url = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/"+o.lang+"."+o.project+"/all-access/user/" + encodeURIComponent(o.title.replace(/ /g,'_')) + "/daily/"+result.options.date+"01/"+result.options.date+daysInMonth(year,month) ;
	}
	
	$('#views'+o.lcnt+"_"+sanitizeID(o.k2)+"_"+result.options.date).html ( "<a href='" + link_url + "' title='See these stats in detail' target='_blank'>" + prettyNumber ( views ) + "</a>" ) ; // FIXME project

	var psid = '#views'+o.lcnt+"_"+sanitizeID(o.k2)+"_sum" ;
	var ps = $(psid).text().replace ( /\D/g , '' ) ;
	$(psid).html ( prettyNumber ( ps*1 + views*1 ) ) ;
	
	var dsid = '#views_total'+o.lcnt+"_"+result.options.date ;
	var ds = $(dsid).text().replace ( /\D/g , '' ) ;
	$(dsid).html ( prettyNumber ( ds*1 + views*1 ) ) ;

	if ( views == 0 ) {
		$('#views'+o.lcnt+"_"+sanitizeID(o.k2)).closest('tr').addClass('zero_views').hide() ;
	} else {
		$('#nonzero'+o.lcnt).text ( prettyNumber ( parseInt($('#nonzero'+o.lcnt).text().replace(/,/g,''))+1 ) ) ;
		$('#totalviews'+o.lcnt).text ( prettyNumber ( sum_views[o.lp] ) ) ;
		if ( languages > 1 ) update_totals() ;
	}
	querying[o.lp]-- ;
	total_querying-- ;
	if ( querying[o.lp] == 0 ) check_queue ( o.lcnt , sum_views[o.lp] , total_querying ) ;

	$("#views_total"+o.lcnt).html ( "<b>" + prettyNumber(sum_views[o.lp]) + "</b>" ) ;
	
//	if ( undefined === wikiDataCache.pageViews[o.url] ) wikiDataCache.pageViews[o.url] = d ;
}

function update_totals () {
	var t_pages = 0 ;
	var t_nonzero = 0 ;
	var t_views = 0 ;
	
	$('.total_pages').each ( function ()   { t_pages += parseInt($(this).text().replace(/,/g,'')) } ) ;
	$('.total_nonzero').each ( function () { t_nonzero += parseInt($(this).text().replace(/,/g,'')) } ) ;
	$('.total_views').each ( function ()   { t_views += parseInt($(this).text().replace(/,/g,'')) } ) ;
	
	var h = "<h4 style='margin-top:10px;padding-top:5px;border-top:1px solid black'>Total</h4><div>" ;
	h += "<span class='label label-info'>" + prettyNumber(t_pages) + " total pages</span> " ;
	h += "<span class='label label-info'>" + prettyNumber(t_nonzero) + " total pages with views</span> " ;
	h += "<span class='label label-info'>" + prettyNumber(t_views) + " total views</span> " ;
	h += "</div>" ;
	
	$('#totals').html ( h ) ;
}

function check_queue ( lcnt , sv , total_querying ) {
	if ( undefined !== lcnt ) {
/*
		var tpid = "table"+lcnt+"_pager" ;
		$('#'+tpid).load ( '../resources/pager.html' , function () {
			$("#table"+lcnt).tablesorter({ sortList: [[1,1]] }).tablesorterPager({container: $("#"+tpid)});
			$('#'+tpid).css ( { position:'' , top:'' } ) ;
		} ) ;
*/
		$("#views_total"+lcnt).html ( "<b>" + prettyNumber(sv) + "</b>" ) ;
		$('#spinner'+lcnt).remove() ;
		$('#table'+lcnt).tablesorter({ sortList: [[1,1]] , textExtraction : tableSorterExtractPrettyNumbers });
//		textExtraction: function(node) { 
	}
	if ( total_querying == 0 ) {
		running = false ;
		updateStatus() ;
		$('#b_run').button('toggle') ;
	}
}

function setYearMonth ( o ) {
	o = $(o) ;
	var year = o.attr('year') ;
	var month = padMonthDay ( o.attr('month') ) ;
	var x = $(o.parents('div.btn-group').get(0)) ;
	x.find('span.year_month').html ( year + '-' + month ) ;
}

function initDates () {
	var h ;
	
	h = '' ;
	for ( var d = 1 ; d <= 31 ; d++ ) {
		h += "<option value='" + d + "'>" + d + "</option>" ;
	}
	$('#start_day').html(h).val('1') ;
	$('#end_day').html(h).val('31') ;
	
	var cy = currentTime.getFullYear() ;
	var cm = currentTime.getMonth() + 1 ;
	h = '' ;
	h += "<li><table>" ;
	for ( var y = 2007 ; y <= cy ; y++ ) {
		h += "<tr>" ;
		h += "<th>" + y + "</th>" ;
		var min_m = y == 2007 ? 12 : 1 ;
		var max_m = y == cy ? cm : 12 ;
		for ( var m = 1 ; m < min_m ; m++ ) h += "<td/>" ;
		for ( var m = min_m ; m <= max_m ; m++ ) {
			h += "<td>" ;
			h += "<a href='#' onclick='setYearMonth(this);return false' year='" + y + "' month='" + m + "'>" ;
			if ( m < 10 ) h += "" ;
			h += m ;
			h += "</a></td>" ;
		}
		for ( var m = max_m ; m <= 12 ; m++ ) h += "<td/>" ;
		h += "</tr>" ;
	}
	h += "</table></li>" ;
	$('#dd_date_start ul').html ( h ) ;
	$('#dd_date_end ul').html ( h ) ;
	
	cm-- ;
	if ( cm == 0 ) { cm = 12 ; cy-- ; }
	$('#end_date').html ( cy+'-'+padMonthDay(cm) ) ;
	cm-- ;
	if ( cm == 0 ) { cm = 12 ; cy-- ; }
	$('#start_date').html ( cy+'-'+padMonthDay(cm) ) ;
}

function init_page() {
//	httpsAlert ( 'result_container' , true ) ;
	initDates() ;
	$('#union').closest('label').popover ( { placement : 'right' , title : 'Category union' , content : 'By default, only pages that are in all given category trees are shown. With this option, all pages in <i>any</i> of the given category trees are shown.' } ) ;
	$('#languages').closest('label').popover ( { placement : 'right' , title : 'Auto-languages' , content : 'This option will find and use corresponding categories in other languages. This can increase the query time considerably.' } ) ;
	$('#dates').closest('label').popover ( { placement : 'right' , title : 'Creation dates' , content : 'This option will retrieve the creation date of each page found. This will take a little longer, depending on your internet connection speed.' } ) ;
	$('#files').closest('label').popover ( { placement : 'right' , title : 'File namespace' , content : 'This option will use the file namespace instead of the article namespace. Intended for Commons.' } ) ;
	updateStatus() ;
}

function initializeFromParameters () {
	var params = getUrlVars() ;
	if ( undefined === params.q ) return ;
	var p = JSON.parse ( decodeURIComponent ( params.q ) ) ;
	
	$.each ( p , function ( k , v ) {
		if ( k == 'depth' ) $('#depth').val ( v ) ;
		else if ( k == 'lang' ) $('#lang').val ( v ) ;
		else if ( k == 'project' ) $('#project').val ( v ) ;
		else if ( k == 'pagepile' ) { $('#pagepile').val ( v ) ; $('#b_run').show() ; }
		else if ( k == 'autolang' ) $('#languages').attr('checked',v) ;
		else if ( k == 'union' ) $('#union').attr('checked',v) ;
		else if ( k == 'files' ) $('#files').attr('checked',v) ;
		else if ( k == 'dates' ) $('#dates').attr('checked',v) ;
		else if ( k == 'rows' ) {
			$.each ( v , function ( k2 , v2 ) {
				var x = {} ;
				if ( undefined !== v2.title ) x.name = v2.title ;
				if ( undefined !== v2.lang ) x.lang = v2.lang ;
				if ( undefined !== v2.project ) x.project = v2.project ;
				if ( undefined !== v2.exclude ) x.exclude = v2.exclude ;
				if ( undefined !== v2.talk ) x.talk = v2.talk ;
				if ( undefined !== v2.depth ) x.depth = v2.depth ;
				addCategory ( x ) ;
			} ) ;
		}
	} ) ;
}

$(document).ready ( function () {
	loadMenuBarAndContent ( { toolname : 'TreeViews' , meta : 'Tree_Views' , content : 'form.html' , run : function () {
		init_page () ;
		initializeFromParameters () ;
	} } ) ;
} ) ;
