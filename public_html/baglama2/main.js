var api = './api.php' ;
var cats = {} ;
var current_month ;
var perma = {} ;

function escattr ( s ) {
	return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#x27;').replace(/\//g,'&#x2F;') ;
}

function ucFirst(string) {
	return string.substring(0, 1).toUpperCase() + string.substring(1);
}

function getUrlVars () {
	var vars = {} ;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1) ;
	var hash = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
	if ( hash == window.location.href ) hash = '' ;
	if ( hash.length > 0 ) hashes = hash ;
	else hashes = hashes.replace ( /#$/ , '' ) ;
	hashes = hashes.split('&');
	$.each ( hashes , function ( i , j ) {
		var hash = j.split('=');
		hash[1] += '' ;
		vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
	} ) ;
	return vars;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function showMonthSite ( gid , year , month , server , giu ) {
	$('#month_site').html ( '<br/><br/><i>loading top view data for ' + server + ', this may take up to a minute for a large number of pages...</i>' ) ;
	perma['giu'] = giu ;
	perma['server'] = server ;

	var last_day_of_month = new Date(year*1, month*1 , 0);
	last_day_of_month = last_day_of_month.getDate() ;
	var year_month = year+'-'+(month*1<10?'0':'')+month ;
	var first_day = year_month+'-01' ;
	var last_day = year_month+'-'+(last_day_of_month<10?'0':'')+last_day_of_month ;

	updateURL() ;
	$.get ( api , { action:'month_site' , gid:gid , year:year , month:month , giu:giu , max:100 } , function ( d ) {
//		console.log ( d ) ;
		var pages = d.data.sort ( function ( a , b ) { return a.views<b.views?1:(a.views>b.views?-1:0) } ) ;
		var h = "<h2>on " + server + "</h2>" ;
		h += "<div>" ;
		if ( pages.length == 100 ) h += "Showing the the top 100 pages only. " ;
		var url = api + "?action=month_site&format=tabbed&gid=" + gid + "&year=" + year + "&month=" + month + "&giu=" + giu ;
		h += "<a target='_blank' href='"+url+"'>Download this table.</a>" ;
		h += "</div>" ;
		h += "<table class='table-condensed table-striped'>" ;
		h += "<thead><tr><th>Page</th><th>Views</th><th>Files</th></thead>" ;
		h += "<tbody>" ;
		$.each ( pages , function ( k , v ) {
			var pageviews_url = 'https://tools.wmflabs.org/pageviews/?project='+server+'&platform=all-access&agent=user&start='+first_day+'&end='+last_day+'&pages=' + encodeURIComponent (v.title) ;
			h += "<tr>" ;
			h += "<td style='vertical-align:top'>" ;
			h += "<a class='external' target='_blank' href='" + pageviews_url + "'>👁</a>&nbsp;" ;
			h += "<a class='external' target='_blank' href='//" + server + "/wiki/" + escattr(v.title) + "'>" + v.title.replace(/_/g,' ') + "</a>" ;
			h += "</td>" ;
			h += "<td style='vertical-align:top' class='num'>" + numberWithCommas(v.views) + "</td>" ;
			var filez = [] ;
			$.each ( v.files , function ( k2 , file ) {
				filez.push ( "<a class='external' target='_blank' href='//commons.wikimedia.org/wiki/File:" + escattr(file) + "'>" + file.replace(/_/g,' ') + "</a>" ) ;
			} ) ;
			h += "<td style='vertical-align:top'>" + filez.join("<br/>") + "</td>" ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		
		$('#month_site').html ( h ) ;
	} ) ;
}

function showMonthStats ( gid , year , month , callback ) {
	perma['month'] = year+''+(month<10?'0'+month:month) ;
	delete perma['giu'] ;
	delete perma['server'] ;
	updateURL() ;
	$('#month_stats').html ( '' ) ;
	$('#month_site').html ( '' ) ;
	$.get ( api , { action:'month_overview' , gid:gid , year:year , month:month } , function ( d ) {
		var sites = d.data.sort ( function ( a , b ) { return a.views<b.views?1:(a.views>b.views?-1:0) } ) ;
		var h = "<h2>Page views in " + year + '-' + (month<9?'0'+month:month) + "</h2>" ;
		h += "<div>Total monthly page views: " + numberWithCommas(current_month[year][month]) + ". " ;
		var url = api + "?action=month_overview&format=tabbed&gid=" + gid + "&year=" + year + "&month=" + month ;
		h += "<a target='_blank' href='"+url+"'>Download this table.</a>" ;
		h += "</div>" ;
		h += "<table class='table-condensed table-striped'>" ;
		h += "<thead><tr><th colspan=2>Site</th><th>Pages</th><th>Views</th></thead>" ;
		h += "<tbody>" ;
		let number_of_sites = 0 ;
		$.each ( sites , function ( k , v ) {
			h += "<tr>" ;
			h += "<td><a class='external' href='//" + v.server + "' target='_blank'>" ;
			if ( v.language == 'wikidata' ) {
				h += "Wikidata" ;
			} else {
				h += ( v.name != null ) ? v.name+' ' : v.language+'.' ;
				h += ucFirst ( v.project ) ;
			}
			h += "</a></td>" ;
			h += "<td><a href='#' class='month_site' giu='" + v.giu + "' server='" + v.server + "' year='"+year+"' month='"+month+"'>Details</a></td>" ;
			h += "<td class='num'>" + numberWithCommas(v.pages) + "</td>" ;
			h += "<td class='num'>" + numberWithCommas(v.views) + "</td>" ;
			h += "</tr>" ;
			number_of_sites++ ;
		} ) ;
		h += "</tbody></table>" ;
		h += "<div>" + number_of_sites + " sites</div>" ;
		$('#month_stats').html ( h ) ;
		if ( typeof callback != 'undefined' ) callback ( gid , year , month ) ;
		$('a.month_site').click ( function () {
			var a = $(this) ;
			var year = a.attr('year') ;
			var month = a.attr('month') ;
			var server = a.attr('server') ;
			var giu = a.attr('giu') ;
			showMonthSite ( gid , year , month , server , giu ) ;
			return false ;
		} ) ;
	} ) ;

	// Logging
	$.getJSON ( 'https://tools.wmflabs.org/magnustools/logger.php?tool=baglama2&method=show catalog/month&callback=?' , function(j){} ) ;
}

function updateURL () {
	var p = [] ;
	$.each ( ['gid','month','giu','server'] , function ( k , v ) {
		if ( typeof perma[v] != 'undefined' ) p.push ( v+'='+perma[v] ) ;
	} ) ;
 	window.location.hash = p.join('&') ;
 }


function showCatStats ( gid , callback ) {
	if ( typeof cats[gid] == 'undefined' ) return ; // TODO error message
	perma = { gid:gid } ;
	updateURL() ;
	$('#catlist').hide();
	$('#cat_stats').html ( "<i>Loading...</i>" ) ;
	$('#month_stats').html ( '' ) ;
	$('#month_site').html ( '' ) ;
	
	$('#main_table tr.highlighted').removeClass ( 'highlighted' ) ;
	$('#main_table tr[gid='+gid+']').addClass ( 'highlighted' ) ;
	
	current_month = {} ;
	$.get ( api , { action:'monthly' , gid:gid } , function ( d ) {
		var fd = [] ;
		var in_prep = [] ;
		var last_year = 0 ;
		var last_month = 0 ;
		$.each ( d.data , function ( year , v1 ) {
			$.each ( v1 , function ( month , v2 ) {
				if ( v2.status != 'VIEW DATA COMPLETE' ) {
					in_prep.push ( year+"-"+(month<10?'0'+month:month) ) ;
					return ;
				}
				if ( typeof current_month[year] == 'undefined' ) current_month[year] = {} ;
				current_month[year][month] = v2.views ;
				if ( year > last_year || ( year == last_year && month*1 > last_month*1 ) ) { last_year = year ; last_month = month ; }
				var date = new Date ( year*1 , month*1-1 ) ;
				var ts = date.getTime() ;
				fd.push ( [ ts , v2.views ] ) ;
			} ) ;
		} ) ;
		fd = fd.sort ( function ( a , b ) { return a[0] < b[0] ? 1 : -1 ; } ) ;
		var h = "<h2>Category details for <a target='_blank' class='external' href='//commons.wikimedia.org/wiki/Category:"+escape(cats[gid].cat)+"'>" + cats[gid].cat + "</a></h2>" ;
		h += "<div>" + numberWithCommas(cats[gid].count) + " months have a data point, with " + numberWithCommas(cats[gid].sum) + " page views in total." ;
		if ( in_prep.length > 0 ) {
			h += " Monthly data for " + in_prep.sort().join(', ') + " is currently being prepared." ;
		}
		h += " Click on individual time points in the graph to see monthly data." ;
		h += "</div>" ;
		h += "<div id='flot1'></div>" ;
		$('#cat_stats').html ( h ) ;
		
		if ( typeof callback != 'undefined' ) {
			callback ( gid , last_year , last_month ) ;
		} else {
			showMonthStats ( gid , last_year , last_month ) ;
		}

		$.plot("#flot1", [fd], {
//			bars: { show: true } ,
			grid: {
				hoverable: true ,
				clickable:true
			},
			points: { show: true } ,
			lines: { show: true } ,
			xaxis: { mode: "time" , minTickSize: [1, "month"],timeformat: "%b %y" }
		});
		
		$("#flot1").bind("plothover", function (event, pos, item) {
			if (item) {
				var x = new Date(item.datapoint[0]), // timestamp
				y = item.datapoint[1]; // value
				
				var xx = x.getMonth() + 1 ;
				xx = x.getFullYear() + '-' + ( xx < 10 ? '0'+xx : xx ) ;

				$("#tooltip").html(xx+": "+numberWithCommas(y)+" views")
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(200);
			} else {
				$("#tooltip").hide();
			}			
		} ) ;
		
		$("#flot1").bind("plotclick", function (event, pos, item) {
			if (item) {
				var date = new Date(item.datapoint[0]) ;
				var month = date.getMonth() + 1 ;
				var year = date.getFullYear() ;
				showMonthStats ( gid , year , month ) ;
			}
		} ) ;
	} ) ;
}

var widar ;

function addCategorySubmit ( e ) {
	e.preventDefault();
	let category = $.trim($('#ac_category').val());
	if ( category == '' ) {
		alert ( "Category must not be empty") ;
		return false ;
	}
	let depth = $('#ac_depth').val() * 1 ;
	let user_name = widar.getUserName() ;
	if ( typeof user_name == 'undefined' || user_name == '' ) {
		alert ( "Not logged in" ) ;
		return false ;
	}
	$.get ( './api.php' , {
		action : 'add_category' ,
		category : category ,
		depth : depth ,
		user_name : user_name
	} , function ( d ) {
		if ( d.status != 'OK' ) {
			alert ( d.status ) ;
			return ;
		}
		if ( d.result == 'added' ) {
			alert ( "Category was added, data for last month will be generated soon (hours)." ) ;
		} else if ( d.result == 'exists' ) {
			alert ( "This category already exists, nothing was changed. Please check the list." ) ;
		} else {
			alert ( "Result :" + d.result ) ;
		}
	} , 'json' ) ;
	return false;
}

$(document).ready ( function () {
/*
	$('#catlist').mouseenter ( function () { $('#catlist').css({'max-height':600}) } )
		.mouseleave ( function () { $('#catlist').css({'max-height':150}) } ) ;
	$('#stats1').mouseenter ( function () { $('#stats1').css({'max-height':'none'}) } )
		.mouseleave ( function () { $('#stats1').css({'max-height':400}) } ) ;
*/

	widar = new WiDaR ( function () {
		$("#checking_login").hide();
		if ( widar.isLoggedIn() ) {
			$("#logged_in").show();
		} else {
			$("#do_log_in_a").html(widar.getLoginLink("log in"));
			$("#do_log_in").show();
		}
	} , '/baglama2/api.php' ) ;

	$("<div id='tooltip'></div>").css({
		position: "absolute",
		display: "none",
		border: "1px solid #fdd",
		padding: "2px",
		"background-color": "#fee",
		opacity: 0.80
	}).appendTo("body");

	$.get ( api , { action:'overview' } , function ( d ) {
		var h = "<table class='table table-striped table-condensed' id='main_table'>" ;
		h += "<thead><tr><th>Category</th><th>Stats</th><th>Months tracked</th><th>Last month</th><th>Total views in all times</th></thead><tbody>" ;
		$.each ( d.data , function ( k , v ) {
			cats[v.gid] = v ;
			let style = '';
			if (v.is_active==0) {
				style = " class='red_mark'";
			}

			h += "<tr gid='" + v.gid + "'>";
			h += "<td"+style+"><a class='external' title='View this category on Commons' target='#blank' href='//commons.wikimedia.org/wiki/Category:" + escattr(v.cat) + "'>" + v.cat + "</a></td>" ;
			h += "<td><a href='#' title='Show details on this category' class='show_stats' gid='" + v.gid + "'>Show</a></td>" ;
			h += "<td class='num'>" + numberWithCommas(v.count) + "</td>" ;
			h += "<td class='num'>" + v.last.substr(0,4) + "-"+v.last.substr(4,2) + "</td>" ;
			h += "<td class='num'>" + numberWithCommas(v.sum) + "</td>" ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		h += "<div>" + d.data.length + " categories total</div>" ;
		$('#catlist').html ( h ) ;
		$('#catlist a.show_stats').click ( function () {
			var a = $(this) ;
			var gid = a.attr('gid') ;
			showCatStats ( gid ) ;
			return false ;
		} ) ;


		var p = getUrlVars() ;
		if ( typeof p.gid != 'undefined' && typeof cats[p.gid] != 'undefined' ) {
			showCatStats ( p.gid , function ( gid , y , m ) {
				if ( typeof p.month != 'undefined' ) {
					y = p.month.substr(0,4) * 1 ;
					m = p.month.substr(4,2) * 1 ;
				}
				showMonthStats ( gid , y , m , function ( gid , year , month ) {
					if ( typeof p.giu != 'undefined' && typeof p.server != 'undefined' ) {
						showMonthSite ( gid , year , month , p.server , p.giu ) ;
					}
				} ) ;
			} ) ;
		}
		
	} , 'json' ) ;
	
} ) ;