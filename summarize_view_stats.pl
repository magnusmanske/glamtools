#!/usr/bin/perl

use strict ;
use warnings ;
use Getopt::Long;

my $date ;
my $result = GetOptions (	"date=s" => \$date, 
) ;

my $dir = '/data/project/glamtools/viewdata' ;
my $fn = "$dir/stats.total" ;

my $prev = '' ;
if ( defined $date ) { # Save other dates
	open IN , $fn ;
	while ( <IN> ) {
		chomp ;
		next if $_ eq '' ;
		my @d = split "\t" , $_ ;
		next if $d[1] eq $date ; # Re-scanning this date
		$prev .= join ( "\t" , @d ) . "\n" ;
	}
	close IN ;
} else {
	$date = '*'
}

open OUT , ">$fn.tmp" ;
print OUT $prev ; # Other dates
foreach my $file ( `ls $dir/$date/*.tab` ) {
	chomp $file ;
	$file =~ m|/([^/]+)\.(\d{4})(\d{2})\.tab$| ;
	my ( $cat , $year , $month ) = ( $1 , $2 , $3 ) ;
	my $total = `awk '{ if(NR>1) a += \$4 } END { print a }' "$file"` * 1;
	print OUT "$cat\t$year$month\t$total\t$file\n" ;
}
close OUT ;

`mv $fn.tmp $fn` ;
