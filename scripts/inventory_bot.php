#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);

require_once ( '/data/project/glamtools/scripts/inventory.php' ) ;
//require_once ( '/data/project/glamtools/public_html/php/wikidata.php' ) ;


$command = $argv[1] ; #'scan' ;

$inventory = new Inventory ;
if ( $command == 'scan' ) {
	$inventory->scanAll () ;
} else if ( $command == 'match_all' ) {
	$inventory->matchAll() ;
} else if ( $command == 'test_scan' ) {
	$q_institution = 'Q657415' ;
	$commons_category == 'Cleveland Museum of Art' ;
	$num = $inventory->checkInstitution ( $q_institution , $commons_category ) ;
	print "{$num}\n" ;
} else if ( $command == 'test_match' ) {
	$inventory->matchFilesForInstitution ( 'Q657415' ) ;
}

#$inventory->tfc->showProcInfo();

?>