#!/usr/bin/perl

use strict ;
use warnings ;
use JSON::XS ;
use Getopt::Long;
use URI::Escape;
use LWP::Simple;
use IO::Handle;
use Encode;
use Data::Dumper ;

my $maxint = 2147483648 ; # 2 ** 31

my $basedir = '/data/project/glamtools/viewdata' ;
my ( $date , $verbose ) ;
my $result = GetOptions (	"date=s" => \$date, 
							"verbose" => \$verbose
) ;

my $usage = "USAGE: pageviews.pl --date YYYYMM\n" ;
die $usage unless defined $date ;
die $usage unless $date =~ /^\d{6}$/ ;

my $dir = "$basedir/$date" ;
mkdir $dir unless -d $dir ;


my @cats ;
open CATS , "$basedir/category.tab" ;
while ( <CATS> ) {
	chomp ;
	next if $_ eq '' ;
	my ( $cat , $depth ) = split /\|/ , $_ ;
	$depth = 5 unless defined $depth ;
	push @cats , [ $cat , $depth ] ;
}
close CATS ;

open LOG , ">$basedir/$date.log" ;

foreach my $ck ( @cats ) {

	my $cat = $ck->[0] ;
	my $depth = $ck->[1] ;
	$cat =~ tr/_/ / ;
	my $fn = "$dir/$cat.$date.tab" ;
	$fn =~ s/'/_/g ;
	if ( -e $fn ) {
		print "File for '$cat' already exists, skipping\n" if $verbose ;
		next ;
	}

	my $fnj = "$fn.json" ;
	my $fnc = "$fn.viewcache" ;
	
	print "$cat [$depth]\n" ;
	
	# Read category tree and image usage data
	my $json = '' ;
	if ( -e $fnj and 0 < -s $fnj ) {
		$json = get_JSON_from_file ( $fnj ) ;
	} else {
		if ( 0 ) { # Use URL
			my $url = "http://tools-webserver-01/catscan2/quick_intersection.php?lang=commons&project=wikipedia&cats=" . uri_escape($cat)  . "&ns=6&depth=" . $depth . "&max=900000&start=0&format=json&giu" ;
			print "Getting JSON category tree and image usage object\n" if $verbose ;
			$json = get ( $url ) ;
			unless ( defined $json ) {
				print LOG "Error (1) when retrieving JSON from $url\n" ;
				next ;
			}
			open OUT , ">$fnj" ;
			print OUT $json ;
			close OUT ;
		} else { # Use command line
			my $cmd = "php /data/project/catscan2/public_html/quick_intersection.php --cats '$cat' --lang commons --project wikimedia --tool glamtools --ns 6 --depth $depth --max 900000 --giu 1 > '$fnj'" ;
			print "$cmd\n" ;
			`$cmd` ;
			print "Result: " . (-s $fnj) . "\n" ;
			unless ( -e $fnj and 0 < -s $fnj ) {
				print LOG "Error (1) when retrieving JSON for $cat\n" ; # LOG
				next ;
			}
			$json = get_JSON_from_file ( $fnj ) ;
		}
		unlink $fnc if -e $fnc ; # View counts file depends on pages list, which depends on this object
	}
	if ( $json eq '' or $json =~ m/Fatal error: Allowed/ ) {
		unlink $fnj if -e $fnj ;
		print LOG "Error (2) when retrieving JSON for $cat\n" ;
		next ;
	}
	$json = decode_json ( $json ) ;
#	print Dumper ( $json->{pages} ) ;
#	exit ;
	
	# Find pages using images
	my %pages ;
	if ( $json->{pagecount} == 0 ) {
		print LOG "No files in $cat\n" ;
		unlink $fnj ;
		next ;
	}
	foreach my $image ( values %{$json->{pages}} ) {
		next unless defined $image->{giu} ;
		foreach ( @{$image->{giu}} ) {
			next unless $_->{ns} == 0 ;
			my $p = uri_escape_utf8 ( $_->{page} ) ;
			next unless $_->{wiki} =~ m/^(.+)wiki$/ ;
			push @{$pages{$1}->{$p}} , $image->{page_title} ;
		}
	}

	# Read existing view stats
	my %views ;
	if ( -e $fnc ) {
		open IN , $fnc ;
		while ( <IN> ) {
			chomp ;
			my ( $wiki , $p , @counts ) = split "\t" , $_ ;
			next unless defined $p ;
			$views{$wiki}->{$p} = \@counts ;
		}
		close IN ;
	}

	# Get view stats from stats.grok.se
	$date =~ m/^(....)(..)/ ;
	my $datebase = "$1-$2-" ;
	open CACHE , ">>$fnc" ;
	CACHE->autoflush;
	foreach my $wiki ( keys %pages ) {
		foreach my $p ( keys %{$pages{$wiki}} ) {
			next if defined $views{$wiki}->{$p} ;
			my $url = "http://stats.grok.se/json/$wiki/$date/$p" ;
			$json = get ( $url ) ;
			my @counts ;
			unless ( defined $json ) {
				@counts = ( 0 ) x 32 ;
			} else {
				$json = decode_json ( $json ) ;
				@counts = ( 0 ) ;
				foreach my $day ( 1 .. 31 ) {
					my $d = $datebase . ( $day < 10 ? "0$day" : $day ) ;
					$counts[$day] = $json->{'daily_views'}->{$d} || 0 ;
					if ( $counts[$day] > $maxint ) {
						$counts[$day] -= $maxint ; #stats.grok.se has the occasional signed 32bit integer overflow...
					}
					$counts[0] += $counts[$day] ;
				}
			
				$views{$wiki}->{$p} = \@counts ;
			}
			print CACHE "$wiki\t$p\t" . join("\t",@counts) . "\n" ;
			print "$wiki\t$p\t" . $counts[0] . "\n" if $verbose ;
		}
	}
	close CACHE ;
	
	# Write data file
	open OUT , ">$fn" ;
	binmode OUT, ":utf8";
	print OUT "Project\tPage\tFile(s)\tTotal" ;
	print OUT "\t$_" foreach ( 1 .. 31 ) ;
	print OUT "\n" ;
	foreach my $wiki ( keys %views ) {
		foreach my $p ( keys %{$views{$wiki}} ) {
			my $page = uri_unescape ( $p ) ;
			my @images = @{$pages{$wiki}->{$p}} ;
			print OUT $wiki . "wiki\t$p\t" . join ( '|' , @images ) . "\t" . join ( "\t" , @{$views{$wiki}->{$p}} ) . "\n" ;
		}
	}
	close OUT ;
	
	unlink $fnj ;
	unlink $fnc ;

	# Generate stats for the month
	`/data/project/glamtools/summarize_view_stats.pl --date $date` ;

}

close LOG ;

sub get_JSON_from_file {
	my ( $fn ) = @_ ;
	my $ret = '' ;
	open IN , $fn ;
	while ( <IN> ) {
		$ret .= $_ ;
	}
	close IN ;
	return $ret ;
}

0 ;
