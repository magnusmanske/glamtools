#!/usr/bin/php
<?php

require_once ( '/data/project/glamtools/baglama2/baglama.php' ) ;

if ( count($argv) < 3 ) {
	die ( "Usage: bg_create_catalog_month.php GROUP YEAR MONTH\n" ) ;
}

$group_id = $argv[1] ;
$year = $argv[2] * 1 ;
$month = $argv[3] * 1 ;

$bg = new Baglama ( 'baglama_removal_script' ) ;
$sql = "SELECT * FROM group_status WHERE group_id={$group_id} AND year={$year} AND month={$month}" ;
$db = $bg->getToolDB() ;
$result = $bg->tfc->getSQL ( $db , $sql ) ;
$gs = $result->fetch_object() ;
if ( !$gs ) die ("No group_status for {$group_id}/{$year}/{$month}\n") ;
print "{$gs->id}\n" ;

$sql = "DELETE FROM gs2site WHERE group_status_id={$gs->id}" ;
$bg->tfc->getSQL ( $db , $sql ) ;

if ( isset($gs->file) ) unlink ( $gs->file ) ;
if ( isset($gs->sqlite3) ) unlink ( $gs->sqlite3 ) ;

$sql = "DELETE FROM group_status WHERE id={$gs->id}" ;
$bg->tfc->getSQL ( $db , $sql ) ;

?>