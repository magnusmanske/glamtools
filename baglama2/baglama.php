<?php

require_once ( '/data/project/glamtools/public_html/php/ToolforgeCommon.php' ) ;

class Baglama {
	private $sqlite_data_root_path = '/data/project/glamtools/viewdata' ;
	private $sqlite_data_tmp_path = '/tmp' ; // '/data/scratch/baglama2' ; // Use this to continue interrupted runs
	private $sqlite_schema_file = '/data/project/glamtools/baglama2/baglama.sqlite3_schema' ;
	public $tfc ;
	private $sites = [] ;
	private $site_id2wiki = [] ;
	private $namespace_cache = [] ;
	private $use_tool_db = false ;

	function __construct ( $toolname = '' ) {
		$this->tfc = new ToolforgeCommon ( $toolname ) ;
		$this->tfc->use_db_cache = false ;
	}

	public function getYearMonthKey ( $year , $month ) {
		$this->sanitizeYearMonth ( $year , $month ) ;
		return $year . ( $month>9 ? $month : "0$month" ) ;
	}

	public function sanitizeYearMonth ( &$year , &$month ) {
		$year *= 1 ;
		$month *= 1 ;
		if ( $year < 2000 or $year > 2030 ) die ( "Unlikely year: $year\n" ) ;
		if ( $month < 1 or $month > 12 ) die ( "Bad month: $month\n" ) ;
	}

	public function constructSqlite3Filename ( $group_id , $year_month ) {
		$group_id *= 1 ;
		if ( $group_id <= 0 ) die ( "Bad group ID: $group_id" ) ;
		if ( !preg_match ( '/^20\d{4}$/' , $year_month ) ) die ( "Bad year/month key: $mear_month\n" ) ;
		$dir = $this->sqlite_data_root_path . '/' . $year_month ;
		@mkdir ( $dir , 0777 , true ) ;
		return "{$dir}/{$group_id}.sqlite3" ;
	}

	public function makeProductionDirectory ( $year , $month ) {
		$year_month = $this->getYearMonthKey ( $year , $month ) ;
		$dir = $this->sqlite_data_root_path . '/' . $year_month ;
		@mkdir ( $dir , 0777 , true ) ;
	}

	public function constructSqlite3TemporaryFilename ( $group_id , $year_month ) {
		$group_id *= 1 ;
		if ( $group_id <= 0 ) die ( "Bad group ID: $group_id" ) ;
		if ( !preg_match ( '/^20\d{4}$/' , $year_month ) ) die ( "Bad year/month key: $mear_month\n" ) ;
		$dir = $this->sqlite_data_tmp_path ;
		@mkdir ( $dir , 0777 , true ) ;
		return "{$dir}/{$year_month}.{$group_id}.sqlite3" ;
	}

	public function openSqlite3File ( $filename , $overwrite = false ) {
		if ( $overwrite and file_exists($filename) ) unlink ( $filename ) ;
		$sqlite3_handle = new SQLite3 ( $filename ) ;
		return $sqlite3_handle ;
	}

	public function addPagesToSqlite3 ( &$sqlite , $group_id , $year , $month ) {
		$group_id *= 1 ;
		$this->sanitizeYearMonth ( $year , $month ) ;
		$this->loadSitesFromSqlite3 ( $sqlite ) ;

		$sqlite->exec ( "DELETE FROM `views`" ) ;
		$sqlite->exec ( "DELETE FROM `group2view`" ) ;

		$offset = 0 ;
		$batch_size = 10000 ;
		do {
			$files = [] ;
			$sql = "SELECT `filename` FROM `files` LIMIT {$batch_size} OFFSET {$offset}" ;
			$result = $sqlite->query ( $sql ) ;
			while($o = $result->fetchArray(SQLITE3_ASSOC)) $files[] = $o['filename'] ;
			$this->addViewsForFilesToSqlite3 ( $sqlite , $group_id , $year , $month , $files ) ;
			$offset += count($files) ;
		} while ( count($files) == $batch_size ) ;
	}

	public function addFilesToSqlite3 ( &$sqlite , $group_id ) {
		$group_id *= 1 ;
		$cd = $sqlite->querySingle ( "SELECT `category`,`depth` FROM `groups` WHERE `id`={$group_id}" , true ) ;
		$sqlite->exec ( 'DELETE FROM `files`' ) ;

		# Get files in category tree from Commons
		$batch_size = 450 ; # sqlite3 limit is 500
		$files = $this->getFilesFromCommonsCategoryTree ( $cd['category'] , $cd['depth'] ) ;
		$sql = [] ;
		foreach ( $files AS $k => $f ) {
			$sql[] = "('" . $sqlite->escapeString($f) . "')" ;
			if ( count($sql) > $batch_size ) {
				$sql = "INSERT INTO `files` (`filename`) VALUES " . implode ( ',' , $sql ) ;
				$sqlite->exec ( $sql ) ;
				$sql = [] ;
			}
		}
		if ( count($sql) > 0 ) {
			$sql = "INSERT INTO `files` (`filename`) VALUES " . implode ( ',' , $sql ) ;
			$sqlite->exec ( $sql ) ;
		}
	}

	public function getFilesFromCommonsCategoryTree ( $category , $depth ) {
		$commons = $this->getCommonsDB() ;
		$files = $this->tfc->getPagesInCategory ( $commons , $category , $depth*1 , 6 , true ) ;
		$commons->close() ;
		return array_keys ( $files ) ;
	}

	public function seedSqlite3File ( &$sqlite , $group_id , $year , $month ) {
		$this->sanitizeYearMonth ( $year , $month ) ;

		# Initialize from schema
		$schema = file_get_contents ( $this->sqlite_schema_file ) ;
		$sqlite->exec ( $schema ) ;

		# Seed with data from MySQL tool DB
		$sqlite->exec ( "BEGIN EXCLUSIVE TRANSACTION" ) ;
		$db = $this->getToolDB() ;
		$seeds = [
			[ 'table'=>'sites' , 'where'=>'' ] ,
			[ 'table'=>'groups' , 'where'=>"`id`={$group_id}" ] ,
			[ 'table'=>'group_status' , 'where'=>"`group_id`={$group_id} AND `year`={$year} AND `month`={$month}" ]
		] ;
		foreach ( $seeds AS $seed ) {
			$table = $seed['table'] ;

			# Clear existing data
			$sql = "DELETE FROM `$table`" ;
			$sqlite->exec ( $sql ) ;
			$sql = "SELECT * FROM `$table`" ;
			if ( $seed['where'] != '' ) $sql .= " WHERE " . $seed['where'] ;
			$result = $this->tfc->getSQL ( $db , $sql ) ;
			while($o = $result->fetch_object()){
				$params = [ [] , [] ] ;
				foreach ( $o AS $k => $v ) {
					$v = $sqlite->escapeString($v) ;
					$params[0][] = "`{$k}`" ;
					$params[1][] = isset($v) ? ($k=='id'?$v:"'{$v}'") : 'null' ;
				}
				$this->insertIntoSqlite3 ( $sqlite , $table , $params ) ;
			}
		}
		$sqlite->exec ( "UPDATE `group_status` SET `status`='',`total_views`=null,`file`=null,`sqlite3`=null" ) ;
		$sqlite->exec ( "COMMIT TRANSACTION" ) ;
		$db->close() ;
	}

	public function getToolDB() { # PERSISTENT CONNECTION
		if ( $this->use_tool_db ) { # Use tooldb
			$db = $this->tfc->openDBtool ( 'baglama2_p' , '' , '' , true ) ;
		} else { # Use trove
			$db = $this->tfc->openDBtrove ( 'baglama2' ) ;
		}
		$db->set_charset("utf8") ;
		return $db ;
	}

	public function getCommonsDB() { # PERSISTENT CONNECTION
		return $this->tfc->openDB ( 'commons' , 'wikimedia' , true , true ) ;
	}


	private function fixServerNameForPageVIewAPI ( $server ) {
		if ( $server == 'wikidata.wikipedia.org' ) return 'wikidata.org' ;
		if ( $server == 'species.wikipedia.org' ) return 'species.wikimedia.org' ;
		return $server ;
	}

	private function getTotalMonthlyPageViews ( $server , $title , $first_day , $last_day ) {
		$server = $this->fixServerNameForPageVIewAPI ( $server ) ;
		$title = urlencode(str_replace(' ','_',$title)) ;
		$url = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/{$server}/all-access/user/{$title}/daily/{$first_day}/{$last_day}" ;
		$data = @file_get_contents ( $url ) ;
		if ( !isset($data) or $data === false or $data == '' ) return 0 ;
		$data = json_decode ( $data ) ;
		$total = 0 ;
		foreach ( $data->items AS $i ) $total += $i->views*1 ;
		return $total ;
	}


	public function addViewCountsToSqlite3 ( &$sqlite , $group_id , $year , $month ) {
		$this->sanitizeYearMonth ( $year , $month ) ;
		$this->loadSitesFromSqlite3 ( $sqlite ) ;

		$ymk = $this->getYearMonthKey ( $year , $month ) ;
		$first_day = $ymk . '01' ;
		$last_day = $ymk . $this->getLastDayOfMonth ( $year , $month ) ;

		# Hide Main Page from view count
		# TODO for all wikis?
		$sql = "UPDATE views SET views=0 WHERE title='Main_Page'" ;
		$sqlite->exec ( $sql ) ;

		$batch_size = 1000 ;

		$found = true ;
		while ( $found ) {
			$found = false ;
			$sql = "SELECT DISTINCT `views`.`id` AS id,title,namespace_id,grok_code,server,namespace_id,done,`views`.`site` AS site_id FROM `views`,`sites` WHERE `done`=0 AND `sites`.`id`=`views`.`site`" ;
			$sql .= " LIMIT {$batch_size}" ;

			$result = $sqlite->query ( $sql ) ;
			while($o = $result->fetchArray(SQLITE3_ASSOC)) {
				$found = true ;
				$wiki = $this->site_id2wiki[$o['site_id']] ;
				$title = $this->prefixWithNamespace ( $o['title'] , $o['namespace_id'] , $wiki ) ;
				if ( !isset($title) ) { // Something is very wrong
					print "Can't prefix '{$o['title']} with NS {$o['namespace_id']} on $wiki (site {$o['site_id']})\n" ;
					$sql = "UPDATE `views` SET `done`=2,`views`=0 WHERE `id`=" . $o['id'] ;
					$sqlite->exec ( $sql ) ;
					continue ;
				}
				$view_count = $this->getTotalMonthlyPageViews ( $o['server'] , $title , $first_day , $last_day ) ;
#print "$wiki: $title: $view_count\n" ;
				$sql = "UPDATE `views` SET `done`=1,`views`={$view_count} WHERE `id`=" . $o['id'] ;
				$sqlite->exec ( $sql ) ;
			}
		}

		$this->addSummaryStatisticsToSqlite3 ( $sqlite , $group_id ) ;
	}

	public function loadNamespacesForWiki ( $wiki ) {
		if ( isset($this->namespace_cache[$wiki]) ) return true ;
		$server = $this->tfc->getWebserverForWiki ( $wiki ) ;
		if ( $server == '' ) return false ; // Can't find server
		$url = "https://$server/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=json" ;
		$j = @file_get_contents ( $url ) ;
		if ( !isset($j) or $j === null or $j == '' ) return false ;
		$j = json_decode ( $j ) ;
		if ( !isset($j) or $j === null or $j == '' ) return false ;
		$this->namespace_cache[$wiki] = [] ;
		foreach ( $j->query->namespaces AS $k => $v ) { // ID => canonical name
			$this->namespace_cache[$wiki][$k] = $v->{'*'} ;
		}
		return true ;
	}

	public function prefixWithNamespace ( $title , $namespace_id , $wiki ) {
		if ( !isset($wiki) ) return ;
		if ( !$this->loadNamespacesForWiki ( $wiki ) ) return ;
		$ns_text = $this->namespace_cache[$wiki][$namespace_id] ;
		if ( !isset($ns_text) ) return ;
		if ( $ns_text == '' ) return $title ;
		return "{$ns_text}:{$title}" ;
	}

	public function finalizeSqlite3 ( &$sqlite , $group_id , $sqlite3_filename ) {
		$group_id *= 1 ;
		$group_status_id = $sqlite->querySingle ( "SELECT id FROM group_status WHERE `group_id`={$group_id}" ) ;
		$total_views = $sqlite->querySingle ( "SELECT total_views FROM group_status WHERE id={$group_status_id}" ) ;
		$sqlite->exec ( "CREATE INDEX `views_views_site_done` ON `views` (`site`,`done`,`views`)" ) ;
		$sqlite->exec ( "CREATE INDEX `g2v_view_id` ON `group2view` (`view_id`)" ) ;
		$this->setGroupStatus ( $group_id , $group_status_id , [
			'status' => 'VIEW DATA COMPLETE' ,
			'total_views' => $total_views ,
			'sqlite3' => $sqlite3_filename
		] ) ;
	}

	// Set $group_status_id=0 to create row
	public function setGroupStatus ( $group_id , $group_status_id , $params ) {
		$db = $this->getToolDB() ;
		if ( $group_status_id == 0 ) {
			$keys = [ '`group_id`'] ;
			$values = [ $group_id*1 ] ;
			foreach ( $params AS $k => $v ) {
				$keys[] = "`{$k}`" ;
				$values[] = "'" . $db->real_escape_string ( $v ) . "'" ;
			}
			$sql = "INSERT IGNORE INTO group_status (" . implode ( ',' , $keys ) . ") VALUES (" . implode ( ',' , $values ) . ")" ;
		} else {
			$sql = [] ;
			foreach ( $params AS $k => $v ) $sql[] = "`{$k}`='" . $db->real_escape_string ( $v ) . "'" ;
			$sql = "UPDATE group_status SET " . implode ( ',' , $sql ) . " WHERE id={$group_status_id}" ;
		}
		$this->tfc->getSQL ( $db , $sql ) ;
		$db->close() ;
	}

	public function getToolSQL ( $sql ) {
		$db = $this->getToolDB() ;
		return $this->tfc->getSQL ( $db , $sql ) ;
	}

# PRIVATE

	private function getLastDayOfMonth ( $year , $month ) {
		$month = ($month>9) ? $month : "0$month" ;
		return date("t", strtotime("$year-$month-01"));
	}

	private function loadSitesFromSqlite3 ( &$sqlite ) {
		if ( count($this->sites) > 0 ) return ; // Done that already
		$sql = "SELECT * FROM `sites`" ;
		$result = $sqlite->query ( $sql ) ;
		while($o = $result->fetchArray(SQLITE3_ASSOC)) {
			$wiki = $this->site2wiki($o) ;
			$this->sites[$wiki] = $o ;
			$this->site_id2wiki[$o['id']] = $wiki ;
		}
	}

	private function site2wiki ( &$site ) {
//			if ( substr ( $language , 0 , 9 ) == 'wikimania' ) $project = 'wikimedia' ; // FIX THIS?
		if ( $site['language'] == 'commons' ) return 'commonswiki' ;
		if ( $site['project'] == 'wikipedia' ) return $site['language'] . 'wiki' ;
		return $site['language'] . $site['project'] ;
	}

	private function addViewsForFilesToSqlite3 ( &$sqlite , $group_id , $year , $month , &$files ) { // Assumes all parameters are sanitized (private function)
		if ( count($files) == 0 ) return ;

		$group_status_id = $sqlite->querySingle ( "SELECT `id` FROM `group_status` WHERE `group_id`={$group_id} AND `year`={$year} AND `month`={$month}" ) ;

		$commons = $this->getCommonsDB() ;

		# Construct SQL to globalimagelinks
		$no_such_filename = '|||' ;
		$sql = [ 'SELECT * FROM `globalimagelinks` WHERE `gil_to` IN (' , "'{$no_such_filename}'" ] ;
		foreach ( $files AS $file ) {
			$sql[] = ",'" . $commons->real_escape_string ( $file ) . "'" ;
		}
		$sql[] = ')' ;
		$sql = implode ( '' , $sql ) ;

		$result = $this->tfc->getSQL ( $commons , $sql ) ;
		while($o = $result->fetch_object()){
			if ( $o->gil_to == $no_such_filename ) continue ; # Paranoia
			
			if ( !isset($this->sites[$o->gil_wiki]) ) {
				print "UNKNOWN WIKI: {$o->gil_wiki}\n" ;
				continue ;
			}

			$params = [
				'site' => $this->sites[$o->gil_wiki]['id'] ,
				'title' => "'" . $sqlite->escapeString($o->gil_page_title) . "'" ,
				'month' => $month ,
				'year' => $year ,
				'done' => 0 ,
				'namespace_id' => $o->gil_page_namespace_id ,
				'page_id' => $o->gil_page ,
				'views' => 0
			] ;

			$sql = [ [] , [] ] ;
			foreach ( $params AS $k => $v ) {
				$sql[0][] = "`{$k}`" ;
				$sql[1][] = $v ;
			}
			$this->insertIntoSqlite3 ( $sqlite , 'views' , $sql ) ;
			
			# Retrieve the inserted view row (last_id won't work, as INSERT IGNORE does not update that)
			$sql = "SELECT id FROM `views` WHERE `site`={$params{'site'}} AND `title`={$params{'title'}} LIMIT 1" ; // Month/year not required, as always the same
			$view_id = $sqlite->querySingle ( $sql ) ;

			$sql = [
				['group_status_id','view_id','image'] ,
				[
					$group_status_id ,
					$view_id ,
					"'" . $sqlite->escapeString($o->gil_to) . "'"
				]
			] ;
			$this->insertIntoSqlite3 ( $sqlite , 'group2view' , $sql ) ;
		}

		$commons->close() ;
	}

	private function addSummaryStatisticsToSqlite3 ( &$sqlite , $group_id ) {
		$group_id *= 1 ;
		$group_status_id = $sqlite->querySingle ( "SELECT id FROM group_status WHERE `group_id`={$group_id}" ) ;
		$sqlite->exec ( "CREATE INDEX `views_site` ON `views` (site)" ) ; # Necessary?
		$sqlite->exec ( "DELETE FROM `gs2site`" ) ;
		$sqlite->exec ( "INSERT INTO `gs2site` SELECT `sites`.id,{$group_status_id},`sites`.id,COUNT(DISTINCT page_id),SUM(views) FROM `views`,`sites` WHERE views.site=sites.id GROUP BY sites.id" ) ;
		$sqlite->exec ( "UPDATE group_status SET status='VIEW DATA COMPLETE',total_views=(SELECT sum(views) FROM gs2site) WHERE id={$group_status_id}" ) ;
	}

	private function insertIntoSqlite3 ( &$sqlite , $table , &$params ) { // Assumes $table is sane, $params is [[keys],[values]], values are escaped and quoted
		$sql = [
			"INSERT OR IGNORE INTO `$table` (" ,
			implode(',',$params[0]) ,
			") VALUES (" ,
			implode(',',$params[1]) ,
			")"
		] ;
		$sql = implode ( '' , $sql ) ;
		$sqlite->exec ( $sql ) ;
	}


} ;


?>